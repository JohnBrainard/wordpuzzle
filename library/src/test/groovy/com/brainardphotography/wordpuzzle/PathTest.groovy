package com.brainardphotography.wordpuzzle

import org.junit.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class PathTest extends Specification {
	def "Path constructor allows valid paths"() {
		when:
		new Path(2, 0, 0).east().southWest().east()
		new Path(2, 1, 1).north().west().south()

		then:
		notThrown IllegalStateException
	}

	def "Path constructor throws exception on invalid path"() {
		when:
		new Path(2, 0, 0, PathMovement.WEST)
		new Path(3, 0, 2, PathMovement.NORTH_EAST, PathMovement.NORTH_EAST, PathMovement.NORTH_EAST)

		then:
		thrown IllegalStateException
	}

	def "getPositions returns correct positions"() {
		when:
		Path path = new Path(2, 0, 0, PathMovement.EAST, PathMovement.SOUTH_WEST, PathMovement.EAST)

		then:
		Position[] expected = [[0, 0], [1, 0], [0, 1], [1, 1]]
		expected == path.positions
	}

	def "getIndexes returns correct indexes"() {
		when:
		Path path = new Path(2, 0, 0, PathMovement.EAST, PathMovement.SOUTH, PathMovement.WEST)

		then:
		path.indexes == [0, 1, 3, 2]
	}

	@Unroll
	def "isValidMovement"(PathMovement movement, Boolean expected, Path path) {
		when:
		Boolean movementValid = path.isValidMovement(movement)

		then:
		expected == movementValid

		where:
		movement                | expected | path
		PathMovement.EAST       | true     | new Path(2, 0, 0)
		PathMovement.WEST       | false    | new Path(2, 0, 0)
		PathMovement.NORTH      | false    | new Path(2, 0, 0)
		PathMovement.SOUTH_WEST | false    | new Path(2, 0, 0)
		PathMovement.NORTH      | false    | new Path(2, 0, 0, PathMovement.EAST, PathMovement.SOUTH, PathMovement.WEST)
	}

	@Unroll
	def "getNextMovementClockwise"(PathMovement start, PathMovement expected, Path path) {
		when:
		PathMovement next = path.getNextMovementClockwise(start)

		then:
		expected == next

		where:
		start              | expected                | path
		PathMovement.NORTH | PathMovement.EAST       | new Path(2, 0, 0)
		PathMovement.NORTH | PathMovement.NORTH_EAST | new Path(2, 0, 1)
		PathMovement.EAST  | PathMovement.SOUTH_EAST | new Path(2, 0, 0)
		PathMovement.NORTH | null                    | new Path(2, 0, 0, PathMovement.EAST, PathMovement.SOUTH, PathMovement.WEST)
	}

	@Unroll
	def "getNextMovementCounterClockwise"(PathMovement start, PathMovement expected, Path path) {
		when:
		PathMovement next = path.getNextMovementCounterClockwise(start)

		then:
		expected == next

		where:
		start              | expected           | path
		PathMovement.NORTH | PathMovement.SOUTH | new Path(2, 0, 0)
		PathMovement.NORTH | PathMovement.EAST  | new Path(2, 0, 1)
		PathMovement.EAST  | PathMovement.SOUTH | new Path(2, 0, 0)
		PathMovement.NORTH | null               | new Path(2, 0, 0, PathMovement.EAST, PathMovement.SOUTH, PathMovement.WEST)
	}


	def "clockwiseMovements"() {
		when:
		Path path = new Path(2, 0, 0)
		Iterator<Path> iterator = path.clockwiseMovements()
		List<Path> paths = iterator.toList()

		then:
		paths.size() == 3
	}

	def "counterClockwiseMovements"() {
		when:
		Path path = new Path(2, 0, 0)
		Iterator<Path> iterator = path.clockwiseMovements()
		List<Path> paths = iterator.toList()

		then:
		paths.size() == 3
	}
}
