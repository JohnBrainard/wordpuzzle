package com.brainardphotography.wordpuzzle

import spock.lang.Specification
import spock.lang.Unroll

import static java.lang.Math.sqrt

class WordPuzzleTest extends Specification {
	@Unroll
	def "WordPuzzle constructor allows valid puzzles"(char[] letters, int[] numbers) {
		when:
		WordPuzzle puzzle = new WordPuzzle(letters, numbers)

		then:
		notThrown IllegalArgumentException
		puzzle.puzzleSize == sqrt(letters.length)

		where:
		letters              | numbers
		['a', 'd', 'd', 'f'] | [4]
		['a', 'd', 'd', 'f'] | [2, 2]
	}

	@Unroll
	def "WordPuzzle constructor throws exception for invalid puzzles"(char[] letters, int[] numbers) {
		when:
		WordPuzzle puzzle = new WordPuzzle(letters, numbers)

		then:
		thrown IllegalArgumentException

		where:
		letters              | numbers
		['a', 'b', 'c']      | [1]
		['a', 'b', 'c', 'd'] | [2]
	}

	def "letterSet returns set with each letter"() {
		when:
		WordPuzzle puzzle = new WordPuzzle(['a', 'd', 'd', 'f'] as char[], [4] as int[])

		then:
		puzzle.letterSet == ['a', 'd', 'f'] as Set<Character>
	}

	def "getCharacterAt returns correct letter"() {
		when:
		WordPuzzle puzzle = new WordPuzzle(['a', 'b', 'c', 'd'] as char[], [4] as int[])

		then:
		puzzle.getCharacterAt(0, 0) == 'a'
		puzzle.getCharacterAt(1, 0) == 'b'
		puzzle.getCharacterAt(0, 1) == 'c'
		puzzle.getCharacterAt(1, 1) == 'd'
	}

	@Unroll
	def "getLetters returns the correct letters for the given path"(char[] letters, char[] expected, Path path) {
		when:
		WordPuzzle puzzle = new WordPuzzle(letters, [letters.length] as int[])

		then:
		puzzle.getLetters(path) == expected

		where:
		letters              | expected             | path
		['a', 'b', 'c', 'd'] | ['a', 'b', 'c', 'd'] | new Path(2, 0, 0, PathMovement.EAST, PathMovement.SOUTH_WEST, PathMovement.EAST)
		['a', 'b', 'c', 'd'] | ['d', 'c', 'a', 'b'] | new Path(2, 1, 1, PathMovement.WEST, PathMovement.NORTH, PathMovement.EAST)
		['a', 'b', 'c', 'd'] | ['d', 'a', 'c', 'b'] | new Path(2, 1, 1).northWest().south().northEast()

	}

	def "getLetterPositions returns the correct values"() {
		when:
		WordPuzzle puzzle = new WordPuzzle(['a', 'd', 'c', 'd'], [4])

		then:
		Position[] expected = [[1, 0], [1, 1]]
		puzzle.getLetterPositions('d' as Character) == expected
	}

	@Unroll
	def "removeLetters returns puzzle with letters removed"(List<Character> letters, Path path, List<Character> expected) {
		when:
		WordPuzzle puzzle = new WordPuzzle(letters, [letters.size()])
		WordPuzzle removed = puzzle.removeLetters(path)

		then:
		println "New letters: ${removed.letters}"
		removed.letters == expected

		where:
		letters                                       | path                                    | expected
		['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'] | new Path(3, 0, 1).east().east()         | [null, null, null, 'a', 'b', 'c', 'g', 'h', 'i']
		['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'] | new Path(3, 0, 1).east().east()         | [null, null, null, 'a', 'b', 'c', 'g', 'h', 'i']
		['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'] | new Path(3, 2, 2).west().north().east() | ['a', null, null, 'd', null, null, 'g', 'b', 'c']
	}
}
