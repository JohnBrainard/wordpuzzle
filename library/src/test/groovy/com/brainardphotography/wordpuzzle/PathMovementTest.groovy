package com.brainardphotography.wordpuzzle

import spock.lang.Specification
import spock.lang.Unroll

class PathMovementTest extends Specification {
	@Unroll
	def "NextClockwise"(PathMovement current, PathMovement expectedNext) {
		when:
		PathMovement next = current.nextClockwise()

		then:
		expectedNext == next

		where:
		current                 | expectedNext
		PathMovement.NORTH      | PathMovement.NORTH_EAST
		PathMovement.NORTH_WEST | PathMovement.NORTH
	}

	@Unroll
	def "NextCounterClockwise"(PathMovement current, PathMovement expectedNext) {
		when:
		PathMovement next = current.nextCounterClockwise()

		then:
		expectedNext == next

		where:
		current            | expectedNext
		PathMovement.NORTH | PathMovement.NORTH_WEST
		PathMovement.SOUTH | PathMovement.SOUTH_EAST
	}

	def "clockwiseIterator"() {
		when:
		Iterator<PathMovement> movementIterator = PathMovement.NORTH.clockwiseIterator()
		List<PathMovement> movements = movementIterator.toList()

		then:
		List<PathMovement> expected = [
				PathMovement.NORTH,
				PathMovement.NORTH_EAST,
				PathMovement.EAST,
				PathMovement.SOUTH_EAST,
				PathMovement.SOUTH,
				PathMovement.SOUTH_WEST,
				PathMovement.WEST,
				PathMovement.NORTH_WEST]

		expected == movements
	}

	def "counterClockwiseIterator"() {
		when:
		Iterator<PathMovement> movementIterator = PathMovement.NORTH.counterClockwiseIterator()
		List<PathMovement> movements = movementIterator.toList()

		then:
		List<PathMovement> expected = [
				PathMovement.NORTH,
				PathMovement.NORTH_WEST,
				PathMovement.WEST,
				PathMovement.SOUTH_WEST,
				PathMovement.SOUTH,
				PathMovement.SOUTH_EAST,
				PathMovement.EAST,
				PathMovement.NORTH_EAST]

		expected == movements
	}
}
