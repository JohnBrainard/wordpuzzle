package com.brainardphotography.wordpuzzle

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import static java.lang.Math.floorDiv

@EqualsAndHashCode
@ToString
class Path {
	private final int startColumn
	private final int startRow
	private final int puzzleSize

	private List<Integer> indexes
	private List<PathMovement> movements

	public Path(int puzzleSize, int startColumn, int startRow, PathMovement... movements) {
		this.puzzleSize = puzzleSize
		this.startColumn = startColumn
		this.startRow = startRow

		this.indexes = createIndexes(movements.toList(), puzzleSize, startColumn, startRow)
		this.movements = movements as ArrayList<PathMovement>
	}

	public List<Position> getPositions() {
		return indexes.collect { new Position(it % puzzleSize, floorDiv(it, puzzleSize)) }
	}

	public Collection<Integer> getIndexes() {
		return indexes
	}

	public Iterator<Path> clockwiseMovements() {
		return movementIterator { PathMovement movement -> getNextMovementClockwise(movement) }
	}

	public Iterator<Path> counterClockwiseMovements() {
		return movementIterator { PathMovement movement -> getNextMovementCounterClockwise(movement) }
	}

	private
	static List<Integer> createIndexes(Collection<PathMovement> movements, int puzzleSize, int startColumn, int startRow) {
		List<Integer> indexes = new ArrayList<>(movements.size() + 1)

		int last = (startRow * puzzleSize) + startColumn
		indexes << last

		movements.each { PathMovement movement ->
			last += movement.colDiff
			last += (movement.rowDiff * puzzleSize)

			if (last < 0 || last >= (puzzleSize * puzzleSize))
				throw new IllegalStateException('Invalid path')
			if (indexes.contains(last))
				throw new IllegalStateException('Movement would cause node to be visited more than once')

			indexes << last
		}

		return indexes.asImmutable()
	}

	public PathMovement getNextMovementClockwise(PathMovement movement) {
		return getNextMovement(movement) { PathMovement m -> m.nextClockwise() }
	}

	public PathMovement getNextMovementCounterClockwise(PathMovement movement) {
		return getNextMovement(movement) { PathMovement m -> m.nextCounterClockwise() }
	}

	public boolean isValidMovement(PathMovement movement) {
		int lastIndex = indexes[-1]

		int col = (lastIndex % puzzleSize) + movement.colDiff
		int row = floorDiv(lastIndex, puzzleSize) + movement.rowDiff

		if (0 <= col && col < puzzleSize && 0 <= row && row < puzzleSize) {
			int index = (row * puzzleSize) + col

			return !indexes.contains(index)
		}

		return false
	}

	public Path move(PathMovement movement) {
		List<PathMovement> movements = this.movements + movement
		return new Path(puzzleSize, startColumn, startRow, *movements)
	}

	public Path north() {
		return move(PathMovement.NORTH)
	}

	public Path northEast() {
		return move(PathMovement.NORTH_EAST)
	}

	public Path east() {
		return move(PathMovement.EAST)
	}

	public Path southEast() {
		return move(PathMovement.SOUTH_EAST)
	}

	public Path south() {
		return move(PathMovement.SOUTH)
	}

	public Path southWest() {
		return move(PathMovement.SOUTH_WEST)
	}

	public Path west() {
		return move(PathMovement.WEST)
	}

	public Path northWest() {
		return move(PathMovement.NORTH_WEST)
	}

	private PathMovement getNextMovement(PathMovement movement, Closure<PathMovement> nextFn) {
		PathMovement next = nextFn.call(movement)

		while (!isValidMovement(next)) {
			next = nextFn.call(next)

			if (next == movement) {
				next = null
				break
			}
		}

		return next
	}

	private Iterator<Path> movementIterator(Closure<PathMovement> nextFn) {
		PathMovement first = nextFn.call(PathMovement.NORTH_WEST)
		PathMovement next = first

		return new Iterator<Path>() {
			@Override
			boolean hasNext() {
				return next != null
			}

			@Override
			Path next() {
				if (next == null)
					throw new NoSuchElementException()

				PathMovement current = next
				next = nextFn.call(next)

				if (next == first)
					next = null

				return move(current)
			}
		}
	}
}
