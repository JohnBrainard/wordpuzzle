package com.brainardphotography.wordpuzzle

import groovy.transform.EqualsAndHashCode


@EqualsAndHashCode
class Position {
	final int column
	final int row

	Position(int column, int row) {
		this.column = column
		this.row = row
	}

	@Override
	String toString() {
		return "($column, $row)"
	}
}
