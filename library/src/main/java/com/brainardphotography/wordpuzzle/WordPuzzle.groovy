package com.brainardphotography.wordpuzzle

import static Math.*

class WordPuzzle {
	private List<Character> letters
	private List<Integer> entries

	final int puzzleSize

	WordPuzzle(List<Character> letters, List<Integer> entries) {
		int cols = floor(sqrt(letters.size()))

		if ((cols * cols) != letters.size())
			throw new IllegalArgumentException('Invalid puzzle.')

		if (entries.sum() != letters.size())
			throw new IllegalArgumentException('Entries count must match total letter count')

		this.letters = letters
		this.entries = entries
		this.puzzleSize = cols
	}

	WordPuzzle(char[] letters, int[] entries) {
		this(Arrays.asList(letters), Arrays.asList(entries))
	}

	List<Character> getLetters() {
		return this.letters.asImmutable()
	}

	/**
	 * Set of unique characters in the puzzle
	 */
	Set<Character> getLetterSet() {
		Set<Character> letterSet = new HashSet<>(letters.size())
		letterSet.addAll(letters)

		return letterSet
	}

	List<Character> getLetters(Path path) {
		return path.positions.collect { getCharacterAt(it.column, it.row) }
	}

	WordPuzzle removeLetters(Path path) {
		List<Character> letters = this.letters.clone()

		path.positions.each {
			letters.set(getCharacterIndex(it.column, it.row), null)
		}

		println "Removed letters: ${letters.join(' ')}"

		for (int i = letters.size() - 1; i >= 0; i--) {
			if (letters.get(i) == null) {
				int n = i
				while ((n -= puzzleSize) >= 0) {
					Character next = letters.get(n)
					if (next != null) {
						letters[i] = next
						letters[n] = null

						break
					}
				}
			}
		}

		return new WordPuzzle(letters, this.entries)
	}

	List<Position> getLetterPositions(Character letter) {
		letters.findIndexValues { it == letter }.collect {
			getCharacterPosition((int)it)
		}
	}

	/**
	 * Look up the character at the specified location
	 * @param column zero-based column index
	 * @param row zero-based row index
	 */
	Character getCharacterAt(int column, int row) {
		return letters[getCharacterIndex(column, row)]
	}

	int getCharacterIndex(int column, int row) {
		return (row * puzzleSize) + column
	}

	Position getCharacterPosition(int index) {
		return new Position(index % puzzleSize, floorDiv(index, puzzleSize))
	}
}
