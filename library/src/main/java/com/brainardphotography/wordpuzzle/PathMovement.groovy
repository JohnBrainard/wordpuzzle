package com.brainardphotography.wordpuzzle

enum PathMovement {
	NORTH(0, -1),
	NORTH_EAST(1, -1),
	EAST(1, 0),
	SOUTH_EAST(1, 1),
	SOUTH(0, 1),
	SOUTH_WEST(-1, 1),
	WEST(-1, 0),
	NORTH_WEST(-1, -1);

	PathMovement(int colDiff, int rowDiff) {
		this.colDiff = colDiff
		this.rowDiff = rowDiff
	}

	int getColDiff() {
		return this.colDiff
	}

	int getRowDiff() {
		return this.rowDiff
	}

	Iterator<PathMovement> clockwiseIterator() {
		PathMovement next = this

		return new Iterator<PathMovement>() {
			@Override
			boolean hasNext() {
				return next != null
			}

			@Override
			PathMovement next() {
				PathMovement current = next

				next = next.nextClockwise()
				if (next == PathMovement.this)
					next = null

				return current
			}
		}
	}

	Iterator<PathMovement> counterClockwiseIterator() {
		PathMovement next = this

		return new Iterator<PathMovement>() {
			@Override
			boolean hasNext() {
				return next != null
			}

			@Override
			PathMovement next() {
				PathMovement current = next

				next = next.nextCounterClockwise()
				if (next == PathMovement.this)
					next = null

				return current
			}
		}
	}

	PathMovement nextClockwise() {
		int nextOrdinal = ordinal() + 1
		if (nextOrdinal == VALUES.length)
			nextOrdinal = 0

		return VALUES[nextOrdinal]
	}

	PathMovement nextCounterClockwise() {
		int nextOrdinal = ordinal() - 1
		if (nextOrdinal < 0)
			nextOrdinal = VALUES.length - 1

		return VALUES[nextOrdinal]
	}

	private static PathMovement[] VALUES = PathMovement.values() as PathMovement[]

	private final int colDiff
	private final int rowDiff
}